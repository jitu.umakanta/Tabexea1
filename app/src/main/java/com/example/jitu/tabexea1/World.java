package com.example.jitu.tabexea1;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class World extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager0;
    RecyclerView.Adapter adapter;


    // View viewpos;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_world, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setHasFixedSize(true);
        layoutManager0 = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager0);
        // requestCount = 1;



        //adapter = new CardAdapter1(listTrndingNewsData9, getActivity());
        //recyclerView.setAdapter(adapter);

        getData();

    }
    private void getData() {
        retrocall("2");
        //requestCount++;
    }

    private boolean isLastItemDisplaying(RecyclerView recyclerView) {
        if (recyclerView.getAdapter().getItemCount() != 0) {
            int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1)
                return true;
        }
        return false;
    }

    public void retrocall(String API_KEY) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<newsresponse> call = apiService.getTopRatedMovies(API_KEY);
        call.enqueue(new Callback<newsresponse>() {
            @Override
            public void onResponse(Call<newsresponse> call, Response<newsresponse> response) {
                int statusCode = response.code();
                List<news> movies = response.body().getArticles();
                adapter = new CardAdapter1(movies, getContext());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<newsresponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });

    }


}
